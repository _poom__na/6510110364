from test.funny_String import funnyString
import unittest

class alternatingCharacters(unittest.TestCase):
    def test_str_ivvkxq_not_funny(self):
        s_list = 'ivvkxq'
        expected_result = 'Not Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)

    def test_str_acxz_funny(self):
        s_list = 'acxz'
        expected_result = 'Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)

    def test_str_holtm_not_funny(self):
        s_list = 'holtm'
        expected_result = 'Not Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)

    def test_str_dsfafw_not_funny(self):
        s_list = 'dsfafw'
        expected_result = 'Not Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)

    def test_str_uvzxrumuztyqyvpnji_funny(self):
        s_list = 'uvzxrumuztyqyvpnji'
        expected_result = 'Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_aaaaa_funny(self):
        s_list = 'aaaaa'
        expected_result = 'Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)

    def test_str_sun_not_funny(self):
        s_list = 'sun'
        expected_result = 'Not Funny'
        result = funnyString(s_list)
        self.assertEqual(expected_result, result)