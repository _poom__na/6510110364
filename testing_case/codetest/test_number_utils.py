from test.number_utils import is_prime_list

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_prime(self):
        prime_list = [1, 2, 3]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_4_5_6_is_not_prime(self):
        prime_list = [4, 5, 6]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_10_20_30_is_not_prime(self):
        prime_list = [10, 20, 30]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_11_22_33_is_not_prime(self):
        prime_list = [11, 22, 33]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_123_456_789_is_not_prime(self):
        prime_list = [123,456,789]
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
    
    def test_give_7_11_13_is_prime(self):
        prime_list = [7, 11, 13]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)
    
    def test_give_19_29_59_is_prime(self):
        prime_list = [19, 29, 59]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)
