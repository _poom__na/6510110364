from test.Two_Characters import alternate
import unittest

class Characters(unittest.TestCase):
    def test_str_beabeefeab_5(self):
        s_list = 'beabeefeab'
        expected_result = 5
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_a_0(self):
        s_list = 'a'
        expected_result = 0
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_ab_2(self):
        s_list = 'ab'
        expected_result = 2
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_asdcbsdcagfsdbgdfanfghbsfdab_8(self):
        s_list = 'asdcbsdcagfsdbgdfanfghbsfdab'
        expected_result = 8
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_princeofsongklauniversity_5(self):
        s_list = 'prince of songkla university'
        expected_result = 5
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_thanachot_3(self):
        s_list = 'thanachot'
        expected_result = 3
        result = alternate(s_list)
        self.assertEqual(expected_result, result)

    def test_str_poomnuek_2(self):
        s_list = 'poomnuek'
        expected_result = 2
        result = alternate(s_list)
        self.assertEqual(expected_result, result)