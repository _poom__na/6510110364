from test.alternating_Characters import alternatingCharacters
import unittest

class alternating(unittest.TestCase):
    def test_str_AA_1(self):
        s_list = 'AA'
        expected_result = 1
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)

    def test_str_AA_BB_VV_3(self):
        s_list = 'AA BB VV'
        expected_result = 3
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_ABBCC_2(self):
        s_list = 'ABBCC'
        expected_result = 2
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)

    def test_str_ABABABAB_0(self):
        s_list = 'ABABABAB'
        expected_result = 0
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_AAAABBAAAA_7(self):
        s_list = 'AAAABBAAAA'
        expected_result = 7
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)

    def test_str_ACV_DB_WAFAHA_0(self):
        s_list = 'ACV DB WAFAHA'
        expected_result = 0
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_AAAABAABBB_6(self):
        s_list = 'AAAABAABBB'
        expected_result = 6
        result = alternatingCharacters(s_list)
        self.assertEqual(expected_result, result)