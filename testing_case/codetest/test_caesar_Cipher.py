from test.caesar_Cipher import caesarCipher
import unittest

class caesar(unittest.TestCase):
    def test_str_middle_Outz_to_okffng_Qwvb(self):
        s_list = 'middle-Outz'
        num = 2
        nums_list = 11
        expected_result = 'okffng-Qwvb'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_Hello_World_to_Lipps_Asvph(self):
        s_list = 'Hello_World!'
        num = 4
        nums_list = 12
        expected_result = 'Lipps_Asvph!'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_wwwabcxy_to_fffjklgh(self):
        s_list = 'www.abc.xy'
        num = 87
        nums_list = 10
        expected_result = 'fff.jkl.gh'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_Ciphering_to_Ciphering(self):
        s_list = 'Ciphering.'
        num = 26
        nums_list = 10
        expected_result = 'Ciphering.'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_159357lcfd_to_159357fwzx(self):
        s_list = '159357lcfd'
        num = 98
        nums_list = 10
        expected_result = '159357fwzx'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_AlwaysLookontheBrightSideofLife_to_FqbfdxQttptsymjGwnlmyXnijtkQnkj(self):
        s_list = 'Always-Look-on-the-Bright-Side-of-Life'
        num = 5
        nums_list = 38
        expected_result = 'Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

    def test_str_Pz_to_Dn(self):
        s_list = 'Pz-/aI/J`EvfthGH'
        num = 66
        nums_list = 16
        expected_result = 'Dn-/oW/X`SjthvUV'
        result = caesarCipher(s_list,num,nums_list)
        self.assertEqual(expected_result, result)

