from test.cat_mouse import cats_and_mouse

import unittest

class CatsAndMouse(unittest.TestCase):
    def test_give_1_2_3_should_cat_b(self):
        cat_a = 1
        cat_b = 2
        mouse_c = 3
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_55_should_cat_b(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 55
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_40_50_30_should_cat_a(self):
        cat_a = 40
        cat_b = 50
        mouse_c = 30
        expected_result = "Cat A"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_0_100_50_should_mouse_c(self):
        cat_a = 0
        cat_b = 100
        mouse_c = 50
        expected_result = "Mouse C"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_0_1_2_should_cat_B(self):
        cat_a = 0
        cat_b = 1
        mouse_c = 2
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_0_0_0_should_mouse_C(self):
        cat_a = 0
        cat_b = 0
        mouse_c = 0
        expected_result = "Mouse C"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)

    def test_give_111_222_333_should_cat_B(self):
        cat_a = 111
        cat_b = 222
        mouse_c = 333
        expected_result = "Cat B"

        result = cats_and_mouse(cat_a, cat_b, mouse_c)

        self.assertEqual(expected_result, result)