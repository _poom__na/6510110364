from test.grid_Challenge import gridChallenge
import unittest

class Characters(unittest.TestCase):
    def test_str_ebacd_fghij_olmkn_trpqs_xywuv_yes(self):
        s_list = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        expected_result = 'YES'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_kc_iu_yes(self):
        s_list = ['kc','iu']
        expected_result = 'YES'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_uxf_vof_hmp_no(self):
        s_list = ['uxf','vof','hmp']
        expected_result = 'NO'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_ppp_ypp_wyw_yes(self):
        s_list = ['ppp','ypp','wyw']
        expected_result = 'YES'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_lyivr_jgfew_uweor_qxwyr_uikjd_no(self):
        s_list = ['lyivr','jgfew','uweor','qxwyr','uikjd']
        expected_result = 'NO'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)
    
    def test_str_l_yes(self):
        s_list = ['l']
        expected_result = 'YES'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)

    def test_str_vbznfwg_eacklwk_bmarzvp_rwgnjqd_xqddtln_wuxtduk_rzmfcik_no(self):
        s_list = ['vbznfwg','eacklwk','bmarzvp','rwgnjqd','xqddtln','wuxtduk','rzmfcik']
        expected_result = 'NO'
        result = gridChallenge(s_list)
        self.assertEqual(expected_result, result)