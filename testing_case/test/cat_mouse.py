# def catAndMouse(x, y, z):
#     a = abs(x-z)
#     b = abs(y-z)
#     if a == b :
#         return 'Mouse C'
#     elif a < b :
#         return 'Cat A'
#     elif a > b :
#         return 'Cat B'
def cats_and_mouse(x, y, z):
    diff_a = abs(x - z)
    diff_b = abs(y - z)

    if diff_a > diff_b:
        return "Cat B"

    elif diff_a < diff_b:
        return "Cat A"

    return "Mouse C"
