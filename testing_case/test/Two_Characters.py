def alternate(s):
    maxnum = 0 
    count = 0
    x = list(set(s))
    
    for i in range(len(x)):
        for j in range(i+1, len(x)):
            l = [x[i],x[j]]
            
            if s.index(x[i]) < s.index(x[j]):
                y = 0
            else :
                y = 1
            for k in s :
                if k in l:
                    if k == l[y]:
                        count += 1
                        y = y ^ 1
                    else :
                        count = 0
                        break
            maxnum = max(maxnum,count)
            count = 0
    return maxnum