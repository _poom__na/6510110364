def caesarCipher(s, k, n):
    temp = []
    
    for i in s :
        temp.append(ord(i))
    
    for j in range(n):
        if 65 <= temp[j] <= 90:
            temp[j] = (65+(temp[j]-65+k)%26)
        elif 97 <= temp[j] <=122:
            temp[j] = (97+(temp[j]-97+k)%26)
    return ''.join(map(chr,temp))
